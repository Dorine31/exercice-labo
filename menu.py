#!/bin/python3
''' création d'un menu textuel, indépendant, pouvant être réutilisé dans un autre programme
'''            

from gestion_labo import *

def menu():
    ''' liste de lignes de menu '''
    return []

def ajouter(m, intitule, commande):
    ''' ajoute chaque ligne dans le menu : intitulé et commande 
    m: liste            => menu 
    intitule: string    => intitulé du menu
    commande: function  => commande du menu correspondant à 1 choix de l'utilisateur
    '''
    m.append([intitule, commande])

def afficher_menu(m): ## menu => [numero, intitule, commande]
    ''' récupère toutes les infos du menu et l'affiche 
    num: int        => indice de la liste menu, permet de numéroter chaque intitulé
    '''
    num = 1
    for num, (intitule, _) in enumerate(m, 1): ##le numéro commence à 1
        print (num, intitule)
    print(0,'Quitter')

def input_choix_menu(m):
    ''' l'utilisateur saisit le choix qu'il fait dans le menu  '''
    while True:
        try: 
            return int(input("Votre choix : "))
        except ValueError:
            print("saisir un nombre entier")

def traiter_choix(m, choix):
    ''' traite le choix que fait l'utilisateur 
    choix: int          => choix du menu
    '''
    # 1 numero, 1 indice sur la liste
    if 1 <= choix <= len(m):
        m[choix-1][1]() ##objet qui est une fonction  : entree =>m[choix-1], cmd =>entree[1], cmd()
    elif choix == 0:
        print("au revoir")
    else:
        print("saisir un chiffre entre 0 et 8")

def gerer_menu(m):
    ''' lance le programme '''
    choix = None
    while choix != 0:
        afficher_menu(m)
        choix = input_choix_menu(m)
        traiter_choix(m, choix)
