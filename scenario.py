#!/bin/python3
'''tests labo'''

from labo import *

def main():
    l = labo()

    enregistrer_arrivee(l, "a", "A101")
    assert l["a"] == "A101"


    enregistrer_arrivee(l, "b", "D404")
    assert l["b"] == "D404"

    #assert taille(l) == 2

    assert enregistrer_depart(l,"a") == "A101"

    modifier_bureau(l, "b", "C204")
    assert l["b"] == "C204"

    changer_nom(l,"b","c")
    assert l["c"] == "C204"

    assert is_member(l,"c") 

    obtenir_bureau(l, "c") 
    assert l["c"]== "C204"

    assert quitter_programme() == "au revoir !"


main()
