#!/bin/python3
#
'''module labo
sous-programmes permettant de proposer un outil de gestion de laboratoire qui recense ses membres et le bureau qu'ils occupent
'''
class LaboException(Exception):
    ''' Généralise les exceptions du laboratoire '''
    pass

class AbsentException(LaboException):
    pass

class PresentException(LaboException):
    pass

import cgi
import json
import csv

def labo():
    '''création du laboratoire, il comprend la liste de toutes les personnes et leurs bureaux respectifs
    fichier: file       => fichier sauvegarde.json 
    '''
    try:
        with open("sauvegarde.json", "r") as fichier:
            return json.load(fichier)
    except:
        return {}

def enregistrer_arrivee(l, personne, bureau):
    '''enregistrer une nouvelle personne travaillant au labo et lui affecte un bureau
       personne: string     => personne membre du laboratoire
       l: dict              => laboratoire
       bureau: string       => numéro du bureau
    '''
    if (personne in l):
        raise PresentException(f"{personne} est déjà enregistré(e)")
    l[personne] = bureau

def taille(l):
    ''' afficher le nombre de personnes présentes au laboratoire, possédant un bureau  '''
    return len(l)

def enregistrer_depart(l,personne):
    ''' supprimer une personne du laboratoire ainsi que l'affectation du bureau'''
    if personne not in l:
        raise AbsentException("personne inconnue")
    return l.pop(personne)

def modifier_bureau(l, personne, bureau):
    '''modifier le bureau occupé par une personne'''
    if personne not in l.keys():
        raise AbsentException("personne inconnue")
    l[personne] = bureau
  
def changer_nom(l, personne, nvelle_personne):
    '''modifier le nom d'une personne du laboratoire
    nvelle_personne: string         => changement de la clé personne
    '''
    if (personne not in l):
        raise AbsentException("personne inconnue")
    if (nvelle_personne in l.keys()):
        raise PresentException("nom déjà saisi")
    l[nvelle_personne] = l.pop(personne)
    return True

def is_member(l, personne):
    ''' vérifier si une personne est membre du laboratoire'''
    return personne in l

def obtenir_bureau(l, personne):
    ''' afficher le nom de la personne affectée à un bureau
    '''
    if personne not in l:
        raise AbsentException("personne inconnue")
    bureau = l[personne]
    return True

def produire_listing(l):
    ''' produire le listing de tous les personnels avec le bureau occupé '''
    #    key: string             => clé du dictionnaire cad personne du laboratoire
    #    value: strign           => valeur du dictionnaire cad bureau du laboratoire
    
    if len(l) == 0:
        raise LaboException("listing vide !")
    for key, value in l.items():
        print(key, value)

def dictionnaire_inverse(l):
    ''' produire le listing de l'occupation des bureaux avec les personnels 
    l_bureaux: dict         => nouveau dictionnaire, reprenant les données inversées de "l" cad ayant pour clé les bureaux et pour valeur les personnes
    '''
    #création d'un dictionnaire dont les clés sont les bureaux (avec multiples valeurs)
    l_bureaux = {}
    for key, value in l.items():
        l_bureaux.setdefault(value, []).append(key)
    if len(l) == 0:
        raise LaboException("listing vide !")
    return l_bureaux

def occupation_bureaux_txt(l_bureaux):
    ''' produire l'occupation des bureau sur le terminal '''
    #    keys: string            => clés du nouveau dictionnaire, cad bureaux
    #    values: string          => valeurs du nouveau dictionnaire, cad personnes
    
    for keys, values in sorted(l_bureaux.items()):
        print(keys)
        for value in sorted(values):
            print(value)

def occupation_bureaux_html(l_bureaux):
    ''' produire l'occupation des bureau en page html '''
    #    page_web: file          => occupation des bureaux générée sous la forme d'une page web
    #    page_html: string       => écriture de la page "occupation des bureaux" en html et stockée sous forme de texte
    #    keys: string            => clés du nouveau dictionnaire, cad bureaux
    #    values: string          => valeurs du nouveau dictionnaire, cad personnes
    #    k: string               => paragraphes de la page web, comportant chaque clé du nouveau dictionnaires
    #    v: string               => paragraphes de la page web, comportant les valeurs pour chaque clé du nouveau dictionnaire
    #    end: string             => écriture de la fin de la page "occupation des bureaux" en html et stockée sous forme de texte
    
    with open('bureaux.html', 'w') as page_web:
        page_html = '''
        <!DOCTYPE html>
        <html>
            <head><meta charset='utf8'></head>
            <body><h1>Occupation des bureaux</h1>'''
        page_web.write(page_html)
        for keys, values in sorted(l_bureaux.items()):
            k = "<p> {}: </p>".format(keys)
            page_web.write(k)
            for value in sorted(values):
                v = "<p> -{} </p>".format(value)
                page_web.write(v)
        end = '''</body>
        </html>'''
        page_web.write(end)

def sauvegarde_labo(l):
    ''' sauvegarde du dictionnaire sous forme json '''
    #    sauvegarde: dict json   => dictionnaire sous format json
    #    fichier_json: file      => fichier json comportant le dictionnaire au format json
    
    if len(l) == 0:
        raise LaboException("fichier vide !")
    sauvegarde = json.dumps(l)
    with open("sauvegarde.json", 'w') as fichier_json:
        fichier_json.write(sauvegarde)

def import_csv(l):
    ''' import d'un fichier csv, ajout des données dans le dictionnaire sauf les doublons et les différences dans les attributions de bureaux  
    '''
    #    fichier_csv: file       => fichier sous format csv
    #    fichier_lu: dict        => données, sous forme d'un OrderedDict, contenues dans le fichier csv
    #    line: string            => valeur dans une rangée de mon OrderedDict
    
    with open('fichier.csv', newline='') as fichier_csv:
        fichier_lu = csv.DictReader(fichier_csv)
        for line in fichier_lu:
            if line['Nom'].upper() not in l:
                l[line['Nom'].upper()] = line['Bureau'].upper()
            else:
                if line['Bureau'].upper() != l[line['Nom'].upper()]:
                    print((line['Nom'], line['Bureau']))
