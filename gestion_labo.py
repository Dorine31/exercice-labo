#!/bin/python3

##classes exception

from labo import*
from menu import*

''' creation d'un laboratoire gérant les personnes qui y travaillent et leur affectation dans un bureau
 '''

def gestion_labo():
    ''' lancement du programme qui utilise les modules menu et labo pour fonctionner 
    l                   => utilisation du laboratoire proposé par labo.py
    m: liste            => menu
    '''
    
    l = labo() #création du laboratoire
    print(l)
    m = menu() #création du menu
    #création des commandes
    ''' personne: string        => nom de la personne membre du laboratoire
        bureau : string         => numéro du bureau affecté à une personne
    '''
    def cmd_arrivee():
        try:
            personne = demander_nom()
            bureau = demander_bureau()
            enregistrer_arrivee(l, personne, bureau)
        except PresentException as e:
            print("erreur", e)
    def cmd_depart():
        try:
            personne = demander_nom()
            enregistrer_depart(l, personne)
        except PresentException as e:
            print("erreur", e)
    def cmd_changer_bureau():
        try:
            personne = demander_nom()
            bureau = demander_bureau()
            print(modifier_bureau(l, personne, bureau))
        except AbsentException as e:
            print("erreur", e)
        except PresentException as e:
            print("erreur", e)
    '''
    nvelle_personne: string => nouveau nom à saisir (changement de nom)
    '''
    def cmd_modifier_personne():
        try:
            personne = demander_nom()
            nvelle_personne = input("saisir le nouveau nom : ")
            nvelle_personne = nvelle_personne.upper()
            print(changer_nom(l, personne, nvelle_personne))
        except AbsentException as e:
            print("erreur", e)
        except PresentException as e:
            print("erreur", e)
    def cmd_verifier_membre():
        try:
            personne = demander_nom()
            print(is_member(l, personne))
        except AbsentException as e:
            print("erreur", e)
    def cmd_obtenir_bureau():
        try:
            personne = demander_nom()
            print(obtenir_bureau(l, personne))
        except AbsentException as e:
            print("erreur", e)
    def cmd_produire_listing():
        try:
            produire_listing(l)
        except LaboException as e:
            print("erreur", e)
    def cmd_occupation_bureaux_txt():
        try:
            l_bureaux = dictionnaire_inverse(l)
            occupation_bureaux_txt(l_bureaux)
        except LaboException as e:
            print("erreur",e)
    '''l_bureaux: dictionnaire      => laboratoire dont la clé est bureau et la valeur est personne
    '''
    def cmd_occupation_bureaux_html():
        try:
            l_bureaux = dictionnaire_inverse(l)
            occupation_bureaux_html(l_bureaux)
        except LaboException as e:
            print("erreur",e)
    def cmd_sauvegarde_labo():
        try:
            sauvegarde_labo(l)
        except LaboException as e:
            print("erreur", e)
    def cmd_import_csv():
        try:
            print("Personnes enregistrées avec numéro de bureau différent : ")
            import_csv(l)
        except LaboException:
            print("erreur")
    #ajout des intitulé et commandes dans le menu
    ajouter(m, "enregistrer une arrivée", cmd_arrivee)
    ajouter(m, "enregistrer un départ", cmd_depart)
    ajouter(m, "changer de bureau", cmd_changer_bureau)
    ajouter(m, "modifier le nom d'une personne", cmd_modifier_personne)
    ajouter(m, "vérifier si une personne est membre du laboratoire", cmd_verifier_membre)
    ajouter(m, "obtenir le bureau d'une personne", cmd_obtenir_bureau)
    ajouter(m, "produire le listing du laboratoire", cmd_produire_listing)
    ajouter(m, "produire l'occupation des bureaux sur le terminal", cmd_occupation_bureaux_txt)
    ajouter(m, "produire l'occupation des bureaux en fichier html", cmd_occupation_bureaux_html)
    ajouter(m, "sauvegarder les données en format json", cmd_sauvegarde_labo)
    ajouter(m, "importer un fichier csv", cmd_import_csv)
    gerer_menu(m)
    sauvegarde_labo(l)

def demander_nom():
    ''' demander le nom de la personne '''
    personne = input("nom de la personne : ")
    personne = personne.upper()
    return personne


def demander_bureau():
    ''' demander le numéro du bureau affecté ou à affecter à une personne '''
    bureau = input("bureau : ")
    bureau = bureau.upper()
    return bureau

if __name__ == "__main__":
    gestion_labo() ##pas de variable globale dans un programme => insérer le "main" dans une fonction
